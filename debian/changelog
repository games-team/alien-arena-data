alien-arena-data (7.71.3+ds-2) UNRELEASED; urgency=medium

  * Remove myself from the uploaders.

 -- Michael Gilbert <mgilbert@debian.org>  Sat, 18 Feb 2023 18:50:24 +0000

alien-arena-data (7.71.3+ds-1) unstable; urgency=medium

  [ Raoul de Raadt ]
  * Team upload.
  * Files-Excluded method for data
  * Added watch file as in alien-arena
  * Fixed issue with Files-Excluded not working
  * Removed separate botinfo, changed some links/email addresses

 -- Raoul de Raadt <raoulderaadt@tiscali.nl>  Sun, 22 Jan 2023 21:32:41 +0100

alien-arena-data (7.66-4) unstable; urgency=medium

  * Team upload.
  * Non-source-only upload since autobuild was not enabled
    for this non-free package.
  * debian/control: Replace ttf-aenigma with fonts-aenigma.
  * debian/control: Bump Standards-Version to 4.5.1.
  * debian/control: Bump debhelper compat to v13.
  * debian/rules: Refresh with dh13 syntax.

 -- Boyuan Yang <byang@debian.org>  Wed, 06 Jan 2021 21:01:48 -0500

alien-arena-data (7.66-3) unstable; urgency=medium

  * Team upload
  * Convert to git
    - d/gbp.conf: Add
    - d/README.source: Mention lack of upstream assets in the git
      repository (they are inconveniently large, so the usual convention
      is not helpful here)
    - d/control: Update Vcs-*
  * Change deprecated Priority: extra to Priority: optional
  * Really use system copy of "Bendable" font from ttf-aenigma
  * Also use system copy of "Crackdown R2" font from ttf-aenigma

 -- Simon McVittie <smcv@debian.org>  Mon, 30 Apr 2018 23:47:12 +0100

alien-arena-data (7.66-2) unstable; urgency=medium

  * Use system ttf-aenigma font.
  * Add missing comma to dependency list.

 -- Michael Gilbert <mgilbert@debian.org>  Sun, 29 Jun 2014 05:57:32 +0000

alien-arena-data (7.66-1) unstable; urgency=medium

  * New upstream release.
  * Replace ttf-freefont dependency with fonts-freefont-ttf (closes: #738215).

 -- Michael Gilbert <mgilbert@debian.org>  Sat, 29 Mar 2014 04:33:57 +0000

alien-arena-data (7.53-1) unstable; urgency=low

  * New upstream release.
  * Bump standards to 3.9.2.

 -- Michael Gilbert <michael.s.gilbert@gmail.com>  Thu, 29 Dec 2011 16:19:34 -0500

alien-arena-data (7.51-2) unstable; urgency=low

  * Team upload.
  * Lower Recommends: alien-arena to Enhances: alien-arena and
    alien-arena-server to avoid a circular dependency, as alien-arena will
    depend on this package.

 -- Bruno "Fuddl" Kleinert <fuddl@debian.org>  Mon, 06 Jun 2011 13:53:11 +0200

alien-arena-data (7.51-1) unstable; urgency=low

  * New upstream release.
  * Convert to source format 3.0 and use new simplified debian/rules format.
  * Bump standards version to 3.9.1.
  * Update watch file.
  * Set up links to use existing free fonts.
  * Set dm-upload-allowed.

 -- Michael Gilbert <michael.s.gilbert@gmail.com>  Sun, 13 Mar 2011 15:44:49 -0400

alien-arena-data (7.40-1) unstable; urgency=low

  * New upstream release
    - Five new maps!
    - Two new weapon models.
    - Improvements to the renderer.
    - Security and memory bugfixes.
    - A variety of gameplay changes and tweaks.
    - Antilag bugfixes.
    - Revamped menu.
  * Update standards version to 3.8.4 (no changes required).
  * Use source format version 3.0 (native).

 -- Michael Gilbert <michael.s.gilbert@gmail.com>  Fri, 07 May 2010 17:50:58 -0400

alien-arena-data (7.33-1) unstable; urgency=low

  * New upstream release.
  * Add support for new upstream file naming scheme to watch file.
  * Updated to standards version 3.8.3.

 -- Michael Gilbert <michael.s.gilbert@gmail.com>  Tue, 29 Dec 2009 17:12:16 -0500

alien-arena-data (7.0-1) unstable; urgency=low

  * New upstream release.
  * New link provided for watch file.
    + Closes: #453556
  * Removing Homepage from Description.
  * Removing XS- prefix from Vcs-* entries.
  * Fixing Vcs-Svn link, removing +ssh part.
  * Bumped Standards-Version to 3.7.3.
  * Using Recommends of alien-arena (>= 7.0) only.
  * Remove README.Debian as information didn't pertain to Debian.
  * Call binary-arch in debian/rules to comply with Debian policy.
  * Test for the existence of build-stamp before attempting to remove.
  * Renamed alien-arena-data-tarball.sh to alien-arena-data-get-orig-source.
  * Modified alien-arena-data-get-orig-source to make it easier to maintain.
  * Removed notice about wrong dm-saucer in copyright file. Doesn't pertain
    to alien-arena-7.0.
  * Cut down package description.
  * Update copyright notice for new release.

 -- Andres Mejia <mcitadel@gmail.com>  Tue, 15 Jan 2008 00:06:37 -0500

alien-arena-data (6.10-1) unstable; urgency=low

  * New upstream release.
  * Added Homepage field in control file.
  * Added XS-Vcs-{Svn,Browser} fields in control file.
  * Adding script to generate orig tarball.

 -- Andres Mejia <mcitadel@gmail.com>  Sat, 10 Nov 2007 20:41:44 -0500

alien-arena-data (6.05-1) unstable; urgency=low

  * Initial packaging (closes: bug#424951)

 -- Andres Mejia <mcitadel@gmail.com>  Thu, 14 Jun 2007 20:42:04 -0400
